import { BrowserRouter, Routes, Route } from "react-router-dom";
import Signin from "./Home/Signin/Signin";
import React, { useState, useEffect } from "react";
import Loadpage from './Home/Loadpage/Loadpage'
import Signup from "./Home/Signup.home/Signup.home";


function Routing() {
    const [loading, useloading] = useState(true)

    useEffect(() => {
        const data = async () => {
            try {
                await new Promise(resolve => setTimeout(resolve, 1000))
                useloading(false)
            } catch (error) {
                console.error('Error: ', error)
                useloading(false)
            }
        }
        data();
    });

    return (
        loading ? (<Loadpage />) :
            (<BrowserRouter>
                <Routes>
                    <Route exact path="/" element={<Signin/>}></Route>
                    <Route exact path="/Signup" element={<Signup/>}></Route>
                </Routes>
            </BrowserRouter>)
    )
}

export default Routing;