export default function Checkbox({ name, value, on_change }) {
    return (
        <label id='checkbox'>
            <input type="checkbox" checked={value} onChange={on_change} />
            {name}
        </label>
    );
}
