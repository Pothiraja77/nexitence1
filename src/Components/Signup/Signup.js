export default function Signupcomponent(SignupFormSubmit){
    return(
        <div className="Body">
            <form onSubmit={SignupFormSubmit}>
                <input type="text" name="Name" placeholder="Enter your Name" required/>
                <input type="email" name="Email"  placeholder="Enter your Email" required/>
                <input type='password' name="New Password" placeholder="Enter your Password" required/>
                <input type="password" name="Confirm Password" placeholder="Enter your Password" required/> 
            </form>
        </div>
    )
}