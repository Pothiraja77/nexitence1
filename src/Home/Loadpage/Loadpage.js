import React, { useState, useEffect } from 'react'
import logo from '../Signin/Logo.png'
import './Loading.scss'


let a = 0;
export default function Loadpage() {

    const [circles, usecircles] = useState({
        cir1: 0,
        cir2: 2,
        cir3: 0
    })

    useEffect(() => {
        const interval = setInterval(() => {
            
            if (a < 8 ) {
                a = a + 1;
                usecircles({ ...circles, cir1: circles.cir1 + 1, cir2: circles.cir2 + 1, cir3: circles.cir3 + 1 })
            } else if ( a >= 8 && a < 16){
                a = a + 1;
                if(a === 16){
                    a = 0;
                }
                usecircles({ ...circles, cir1: circles.cir1 - 1, cir2: circles.cir2 - 1, cir3: circles.cir3 - 1 })
                
            }


        }, 50);
        return () => clearInterval(interval)
    }, [circles.cir1, circles.cir2, circles])


    return (
        <div className='loadbody'>
            <img src={logo} alt='Loading' />
            <svg>
                <circle cx="20" cy="50%" style={{ r: `${circles.cir1}` }} fill="white" />
                <circle cx="50" cy="50%" style={{ r: `${circles.cir2}` }} fill="white" />
                <circle cx="80" cy="50%" style={{ r: `${circles.cir3}` }} fill="white" />
            </svg>
        </div>
    )
}
