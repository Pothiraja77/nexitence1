import './Signin.scss'
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie'
import validation from './validation';
import { beforecolors, aftercolors, inputdata } from './signin_format';
import Checkbox from '../../Components/checkbox';
import signinpic from './Signin.png'
import Logo from './Logo.png'
import Sun from './sun.svg'
import moon from './moon.svg'

export default function Signin() {

    const navigate = useNavigate()

    const [error, setError] = useState({});
    const [datavalue, setData] = useState({
        email: "",
        password: "",
    });
    const h_Change = (e) => {
        setData({ ...datavalue, [e.target.name]: e.target.value });
    };
    const h_Submit = (event) => {
        event.preventDefault();
        const validationErrors = validation(datavalue);
        setError(validationErrors);
        

        if (validationErrors === '') {
            console.log(JSON.stringify(datavalue));
            Cookies.set('email', datavalue.email)
            Cookies.set('password', datavalue.password);
            navigate('/Signup');
            
        }
        if (checked) {
            localStorage.setItem('remember_value', JSON.stringify(datavalue));
        }
    };

    const [background, usebackground] = useState({})

    const datamap = inputdata.map((data) => (<div key={data.num}>
        <label id={data.label} >{data.name}
            <input
                id={data.id}
                style={{ background: `${background.light_color}`, border: `${error.email ? '1.5px solid red' : ''}` || `${error.password ? '1px solid red' : ''}` }}
                type={data.type}
                name={data.name}
                placeholder={data.placeholder}
                onChange={h_Change} required={data.req}
            />
            <p>
                {error.email ? data.err : ''}
                {error.password ? data.pas : ''}
            </p>
        </label>
    </div>
    )
    )

    const [checked, usechecked] = useState(false)

    const handleChange = () => {
        usechecked(!checked)
    }

    const [theme, usetheme] = useState(<img src={moon} alt='moon' />)
    const [tm, usetm] = useState(true)
    const color = () => {
        if (tm === true) {
            usetheme(<img src={Sun} alt='light' />)
            usebackground({
                ...background,
                b_color: `${aftercolors.bgcolor}`,
                button_color: `${aftercolors.submitcolor}`,
                text_color: `${aftercolors.t_color}`,
                light_color: `${aftercolors.l_color}`,
                linkcolor: `${aftercolors.link}`
            })
            usetm(false)
        } else if (tm === false) {
            usetheme(<img src={moon} alt='moon' />)
            usebackground({
                ...background,
                b_color: `${beforecolors.bgcolor}`,
                button_color: `${beforecolors.submitcolor}`,
                text_color: `${beforecolors.t_color}`,
                light_color: `${beforecolors.l_color}`,
                linkcolor: `${beforecolors.link}`
            })
            usetm(true)
        }
    }

    const signuppage = () => {
        navigate(<div>hello</div>)
    }

    return (
        <body style={{ background: `${background.b_color}` }}>
            <div onClick={color} style={{ cursor: 'pointer' }} id='theme'>{theme}</div>
            <img src={Logo} alt='logo' />
            <form onSubmit={h_Submit} className='signin_form' style={{ color: `${background.text_color}` }}>
                <h1 className='logintitle'>Login now</h1>
                {datamap}
                <div className='checkandforgot'>
                    <Checkbox name='Remember me' value={checked} on_change={handleChange} />
                    <div id='forgotpass' ><span style={{ color: `${background.linkcolor}`, cursor: 'pointer' }}>Forgot password</span> ?</div></div>
                <button id='signin_sub' style={{ background: `${background.button_color}` }}>Submit</button>
                <div id='signup'>Not registered yet ? <span onChange={signuppage} style={{ color: `${background.linkcolor}`, cursor: 'pointer' }}>Create an account.</span></div>
            </form>
            <img src={signinpic} alt='signin'></img>
        </body>
    );
}
