export default function validation(data){
    let errors = {};
    let emailerr = /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/;
    let passworderr = /^(?=.*[a-z0-9A-Z])(?!.*[!@#$%^&*()]).{8,16}$/;

    if (!emailerr.test(data.email)) {
        errors.email = true;
    }
    if (!passworderr.test(data.password)){
        errors.password = true;
    }
    return errors;
};