export const beforecolors = { 
    bgcolor: 'white',
    submitcolor:'#E8505B',
    t_color: 'black',
    l_color: '#E8505B40',
    link : '#00008D'
}
export const aftercolors = {
    bgcolor: '#222',
    submitcolor: '#E8505B',
    t_color: 'white',
    l_color: 'white',
    link:'#1a43bf'
}


export const inputdata = [
    {
    num: 1, 
    label:'maildiv', 
    id: 'email', 
    type: 'email', 
    name: 'Email', 
    placeholder: 'Enter your Email', 
    req: true, 
    err:'Invalid email address' 
},
    { 
    num:2,
    label: 'passdiv',
    id: 'pass', 
    type: 'password', 
    name: 'Password', 
    placeholder: 'Enter your Password', 
    req: true, 
    pas:'Invalid password' }
]
